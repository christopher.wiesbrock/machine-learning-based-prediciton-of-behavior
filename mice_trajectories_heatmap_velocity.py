# -*- coding: utf-8 -*-
"""
Created on Wed Sep 13 15:23:53 2023

@author: lilli
"""

# -*- coding: utf-8 -*-
"""

@author: lilli
"""
import cv2
import pandas as pd
import numpy as np
from glob import glob
import re
import time
import matplotlib.pylab as plt
import matplotlib as mpl
import seaborn as sns
from scipy.spatial import distance

"""
"""
# functions to load data
# load data participant
#value to define = m_id

m_id = 8 #set mouse id

mouse_id = str(2690+m_id)
tframe= 7.005732042739065690e+00
xt = tframe
pl = 0.9
pos = m_id-2
del_i = 2* pos

def load_participant (x):
    path=pd.DataFrame(glob("C:/Users/lilli/Documents/Bildung/Uni/aachen/Unterricht_Inhaltlich/AG Kampa/Hallway Illusion/data/behavior_data/sorted_2/"+str(x)+"/*"))
    path.columns=['location']
    
    d={}
    for i in range (len(path['location'])):
        da=pd.read_csv(str(path['location'].iloc[i]))
        
        p=time.strptime(str(da['date'][0]), '%Y_%b_%d_%H%M' )
        dr=[]
        for r in range (len(da)):
            dr.append(np.cumsum(da['touch_delay']))
        da=da.join(pd.DataFrame({'frames_total': dr}))
        
        d[str(x)+'_0'+str(p[1])+str(p[2])+'_'+str(p[3])+str(p[4])]=da
        
    keys = [k for k, v in d.items()]
        
    return d, keys

def load_track (x):

    path=pd.DataFrame(glob("C:/Users/lilli/Documents/Bildung/Uni/aachen/Unterricht_Inhaltlich/AG Kampa/Hallway Illusion/data/tracked_results_DLC/mice_sorted2/"+str(x)+"/*"))
    path.columns=['location']
    
    p=path
    p.loc[:]=p.loc[:].astype(str)

    r=p['location'][0]
    f=r.split('_')

    c=[]
    for i in range (len(p['location'])):
        a=p['location'][i]
        f=re.split('_|2023|(\d+)',a)
        c.append(f[15])
        
    d={}
    for i in range (len(path['location'])):
        da=pd.read_csv(str(path['location'].iloc[i]))
        d[str(x)+'_'+str(c[i])]=da
    
    keys = [k for k, v in d.items()]

    return d, keys

# allignment behavior, DLC data

def touch_point_delay (t_script, mouse_ID, pos):
    d_mouse, keys_behavior = load_participant(mouse_ID)
    track_mouse, keys_track = load_track (mouse_ID)
    
    # load video to get framerate    
    path=pd.DataFrame(glob(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\Hallway Illusion\data\maus_videos\mixed\*"))
    path.columns=['location']
    cap=cv2.VideoCapture(str(path['location'].iloc[pos]))
    fps= float(cap.get(cv2.CAP_PROP_FPS)) #get frame rate of original video
    
    touch=d_mouse[keys_behavior[0]]
    touch_delay=touch['touch_delay']
    lick_delay=touch['lick_delay']
    
    touch_points=[]
    
    for r in range (len(touch_delay)): 
        if r==0:
            a=0
        else:
            a= np.sum(lick_delay[0:r])
            
        b = touch_delay[r]
        touch_points.append(a+b)
        
        touch_track = np.array(touch_points) + t_script
        touch_t= touch_track*fps
    
    return touch_points, touch_t, keys_track, track_mouse, touch, fps
    
# load data    
# allignment behavior, DLC data
#delay

def load_delay(r):
    path=pd.DataFrame(glob(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\Hallway Illusion\data\merged_behavior_tracked\*"))
    path.columns=['location']
    x_id= str((path.iloc[r])[0])
    
    seq= pd.read_csv(str(x_id))
    seq= seq.columns
    seq= seq[1:-1].astype('float')[0]
    #fps = seq[2]
    
    return seq #,fps

delay = load_delay(del_i)
touch_points, touch_t, keys_track, track_mouse, d_mouse, fps = touch_point_delay( delay, mouse_id, pos)
de = delay/fps


# frame rate videos
track_mouse= track_mouse[keys_track[0]] # get mouse_data via key
#plot trajectories and heatmap
#right/ left choice, correct/wrong choice data
like_bp1 = track_mouse.iloc[:,6]
like_bp2 = track_mouse.iloc[:,9]

def choice_data (delay, d_mouse, track_mouse, choice, i_col):
    
    like_bp1 = track_mouse.iloc[2:,6].astype(float)
    like_bp2 = track_mouse.iloc[2:,9].astype(float)
    
    touch_delay = d_mouse['touch_delay']
    lick_delay = d_mouse['lick_delay']
    
    touch_points=[]
    
    for r in range(len(touch_delay)):
        if r==0:
            a=0
        else:
            a= np.sum(lick_delay[0:r])
        
        b = touch_delay[r]
        touch_points.append(a+b + delay)
    
    d_touch = []
    
    for r in range (len(touch_points)):
        if d_mouse.iloc[r, i_col] == choice:
            d_touch.append(int(touch_points[r]*fps))
    
    
    x = []
    xe=[]
    y = []
    ye = []
    #l = []
    for r in range (len(d_touch)):
        #a = int(start[r]+2)
        b = int(d_touch[r]+2)
        d = b
        c = int(b-xt*fps)
        if c >= 0:
            xa = track_mouse.iloc[c:d, 4].astype(float)
            xa = xa[like_bp1>pl][like_bp2>pl]  
            x.append(xa)
            xa = track_mouse.iloc[c:d, 7].astype(float)
            xa = xa[like_bp1>pl][like_bp2>pl]  
            xe.append(xa)
            xa = track_mouse.iloc[c:d, 5].astype(float)
            xa = xa[like_bp1>pl][like_bp2>pl]  
            y.append(xa)
            xa = track_mouse.iloc[c:d, 8].astype(float)
            xa = xa[like_bp1>pl][like_bp2>pl] 
            ye.append(xa)
    xp=[]
    yp=[]
    for r in range(len(x)):
        dist_x = x[r] - xe[r]
        dist_y = y[r] -ye[r]
        p_x = x[r] + 0.5 * dist_x
        p_y = y[r] + 0.5 * dist_y
        xp.append(p_x)
        yp.append(p_y)
        
            
    return xp, yp

correct_x, correct_y = choice_data(delay, d_mouse, track_mouse, 1, 5)
wrong_x, wrong_y = choice_data(delay, d_mouse, track_mouse, 0, 5)
left_x, left_y = choice_data(delay, d_mouse, track_mouse, 'left', 11)
right_x, right_y = choice_data(delay, d_mouse, track_mouse, 'right', 11)        



#plotting
#trajectory

def plotting(data_x, data_y, ax1, ax2, label):
    x = []
    y=[]
    for r in range(len(data_x)):
        x = data_x[r][:]
        y = data_y[r][:]
        
        axes[ax1,ax2].plot(x, y, linewidth=0.3, label=label, color='black')
        axes[ax1,ax2].set_title(label)
        if ax1 == 1:
            axes[ax1,ax2].set_xlabel('X-coordinates')
        if ax2 == 0:
            axes[ax1,ax2].set_ylabel('Y-coordinates')
    return 

plt.figure()
fig, axes = plt.subplots(2,2, sharey=True, sharex=True, figsize=(10, 5))
fig.tight_layout(h_pad=2)
font = {'size': 14, 'sans-serif':'Arial'} # define font
mpl.rc('font', **font) # use font
plotting(correct_x, correct_y, 0,0, 'Correct Trials')
plotting(wrong_x, wrong_y,0,1, 'Wrong Trials')
plotting(left_x, left_y, 1,0, 'Touch Right')
plotting(right_x, right_y, 1,1, 'Touch Left')
axes[0,0].text(-0.2, 1.05, 'A', font={'size':16} ,transform=axes[0,0].transAxes)
sns.despine()

#plt.savefig(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\images\Hallway\like_filter_90perc\trajectories/"+ str(mouse_id)+".png", dpi=300,  bbox_inches='tight')
#plt.savefig(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\images\Hallway\like_filter_90perc\trajectories/"+ str(mouse_id)+".svg", dpi=300,  bbox_inches='tight')


#heatmap
#histogram 

def plot_hist (data_x, data_y, ax1, ax2, label):
    
    cx = []
    cy = []
    for r in range(len(data_x)):
        x = data_x[r]
        y = data_y[r]
        for k in range (len(x)):
            cx.append(x.iloc[k])
            cy.append(y.iloc[k])
        
    
    axes[ax1, ax2].hist2d(cx,cy, bins=[20, 20], range=np.array([(750, 1600), (0, 1600)]), cmap='YlGnBu_r')
    axes[ax1,ax2].set_title(label)
    # using rc function
    if ax1 == 1:
       # mpl.rc('font', **font_a)
        axes[ax1,ax2].set_xlabel('X-Coordinates')
    if ax2 == 0:
       # mpl.rc('font', **font_a)
        axes[ax1,ax2].set_ylabel('Y-Coordinates')
    
    return

plt.figure()
fig, axes = plt.subplots(2,2, sharey=True, sharex=True, figsize=(12, 6))
fig.tight_layout(h_pad=2)
plt.subplots_adjust(wspace=0.15) # distance between plots, width

# parameters legend
N = 21
norm = mpl.colors.Normalize(vmin=0,vmax=2)
sm = plt.cm.ScalarMappable( norm=norm, cmap = 'YlGnBu_r')
sm.set_array([])
fig.colorbar(sm, ticks=np.linspace(0,4,N), 
             boundaries=np.arange(0,2.1,.1), ax = axes[:,:])

font = {'size': 14, 'sans-serif':'Arial'} # define font
mpl.rc('font', **font) # use font
plot_hist(correct_x, correct_y, 0,0, 'Correct Trials')
plot_hist(wrong_x, wrong_y,0,1, 'Wrong Trials')
plot_hist(left_x, left_y, 1,0, 'Touch Right')
plot_hist(right_x, right_y, 1,1, 'Touch Left')
axes[0,0].text(-0.2, 1.05, 'B', font={'size':16} ,transform=axes[0,0].transAxes)

#plt.savefig(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\images\Hallway\like_filter_90perc\heatmap/"+ str(mouse_id)+".png", dpi=300,  bbox_inches='tight')
#plt.savefig(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\images\Hallway\like_filter_90perc\heatmap/"+ str(mouse_id)+".svg", dpi=300,  bbox_inches='tight')


#velocity plots

#velocity
def get_velocity (data_x, data_y):
#data_x = correct_x
#data_y = correct_y
    
    dt_0= 1/fps
    i = tframe
    vel = [] # for every goal: mean velcoity of every second: nr_goals * 17 * mean_vel
    
    for m in range(len(data_x)):
        v = []
        if len(data_x[m]) > i: #if time till touch is longer than 7 sec

            for s in range (int(i)):
                l = (1/i) * len(data_x[m])
                i1 = int(s*l)
                i2 = int(i1 + l)
                cx_p = (data_x[m][i1:i2]) # x_values sorted into seconds : l * x_values
                cy_p = (data_y[m][i1:i2]) # y_values sorted into seconds : l * y_values
                
                vv = []
                for k in range(len(cx_p)-1):
                    a = cx_p.iloc[k], cy_p.iloc[k]
                    b = cx_p.iloc[k+1], cy_p.iloc[k+1]
                    d = distance.euclidean(a,b) # get euclidean distance of every tuple
                    vv.append(d/dt_0) # calculate and append velocity
                    
                v.append(np.mean(vv)) # calculate and append mean velocity of every second
        vel.append(v) # append mean_velocities
    
    # calculate mean velocity of every second over all goals
    mean_speed = []
    t = []
    for z in range(int(i)):
        e  = []
        for y in range(len(vel)):
            e.append(vel[y][z])
        mean_speed.append(np.mean(e)) #append mean velocity in mean_speed 2times
        mean_speed.append(np.mean(e))
        t.append(z)
        t.append(z+1)
            
    return mean_speed, t, vel

def plot_vel (dx, dy, ax1, ax2, label):
     
    goals_vel, t, vel = get_velocity(dx, dy)
    
    
    axes[ax1, ax2].plot(t, goals_vel, color = 'black')
    axes[ax1,ax2].set_title(label)
    
    if len(vel) ==0 :
        message = 'No Goals'
        axes[ax1, ax2].text(7.0, 750, message)
    # using rc function
    if ax1 == 1:
       # mpl.rc('font', **font_a)
        axes[ax1,ax2].set_xlabel('Time [s]')
    if ax2 == 0:
       # mpl.rc('font', **font_a)
        axes[ax1,ax2].set_ylabel('Velocity [px/s]')
        
    return
    


plt.figure()
fig, axes = plt.subplots(2,2, sharey=True, sharex=True, figsize=(9, 6))
fig.tight_layout(h_pad=2)
font = {'size': 14, 'sans-serif':'Arial'} # define font
mpl.rc('font', **font) # use font          

plot_vel(correct_x, correct_y, 0,0, 'Correct Trials')
plot_vel(wrong_x, wrong_y,0,1, 'Wrong Trials')
plot_vel(left_x, left_y, 1,0, 'Touch Right')
plot_vel(right_x, right_y, 1,1, 'Touch Left')
axes[0,0].text(-0.2, 1.05, 'C', font={'size':16} ,transform=axes[0,0].transAxes)

sns.despine()

#plt.savefig(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\images\Hallway\like_filter_90perc\velocity/"+ str(mouse_id)+".png", dpi=300,  bbox_inches='tight')
#plt.savefig(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\images\Hallway\like_filter_90perc\velocity/"+ str(mouse_id)+".svg", dpi=300,  bbox_inches='tight')


