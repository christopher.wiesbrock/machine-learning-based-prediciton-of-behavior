# -*- coding: utf-8 -*-
"""
Created on Thu Aug 31 16:41:30 2023

@author: lilli
"""
import cv2
import numpy as np
from glob import glob
import pandas as pd
import re


path=pd.DataFrame(glob(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\Hallway Illusion\data\maus_videos\mixed\*"))
path.columns=['location']

def load_video (r):
    x= str((path.iloc[r])[0])
    
    vidcap = cv2.VideoCapture(str(x))
    fps= float(vidcap.get(cv2.CAP_PROP_FPS)) #get frame rate of origina video
     # get length of original video
    #vid_length = int(vidcap.get(cv2.CAP_PROP_FRAME_COUNT))
    
    def getFrame(sec):
        vidcap.set(1,sec)
        #vidcap.set()
        hasFrames,image = vidcap.read()     # save frame as JPG file
        return hasFrames, image

    corr=[]
    correl=[]
    fp=[]
    fer=[]
    for sec in range(int(fps*750)): #get onsets for 12 minutes
        success, img = getFrame(fps*180+sec) #load frames, starting at 180seconds
        
        img = img[0:1000, 0:600] # define ROI
        
        img2= cv2.cvtColor(img, cv2.COLOR_BGR2GRAY) # convert frame to grayscale
        hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV) # convert frame to hsv color space
        
        ret, img2 = cv2.threshold(img2, 235, 255, cv2.THRESH_BINARY) #set black and white threshold
        ref, hsv2 = cv2.threshold(hsv, 200, 255, cv2.THRESH_BINARY) 
        
        a=img2[img2>0]
        b= hsv2[hsv2>0]
        
       
        #select frames with multiple white particles
        if (np.max(img2)-np.min(img2))>100 and len(a)<2000 and len(a)>40:
            correl.append(1)
            if correl[sec-1]==0:
                fp.append(sec/fps+180)
        else: 
            correl.append(0)
    
        if (np.max(hsv2)-np.min(hsv2))>100 and len(b)>700:
            corr.append(1)
            if corr[sec-1]==0:
                fer.append(sec/fps+180)
        else: 
            corr.append(0)
            
    #compare selection from both thresholdings
    con=[0, fps]
    for r in range(len(fer)):
        for l in range(len(fp)):
            if fer[r]==fp[l]:
                a= fer[r]
                con.append(a)
    con.append(0)
    
    mi = re.split("maus_videos|1608|Pro", x)
    mouse_path = str(mi[0]) + 'merged_behavior_tracked'+str(mi[2]) #get mouse_id + date as title
    #save comparison/ lamp onsets as csv 
    file = open(str(mouse_path)+'.csv','w') 

    file.writelines(str(con[:]))
    file.close()
    
    return con, mi
# get onsets from all files in folder

con, mi= load_video(7)



    
