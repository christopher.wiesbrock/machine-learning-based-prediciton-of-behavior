# -*- coding: utf-8 -*-
"""
Created on Sat Sep 16 02:18:33 2023

@author: lilli
"""


import pandas as pd
import numpy as np
import matplotlib.pylab as plt

import matplotlib as mpl
import seaborn as sns

fps = 24

# function to get float values from goal cvs 
def get_goals_float (goals):
    g = goals.columns
    if len(g) > 1:
        
        g_1= [float(g[0].rsplit('[',1)[-1])]
        g_2 = [float(g[-1].rsplit(']',1)[0])]
        goals= g[1:-1].astype('float').tolist()
        goals = np.concatenate((g_1, goals, g_2))
    else:
        g = g[0].rsplit('[',1)[1]
        g = g.rsplit(']',1)[0]
        if g != '':
            goals = [float(g)]
        else:
            goals = []
    return goals

# load goals
def load_goals (id):
    goals_black = pd.read_csv(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\Foosball\goals\24_fps\g"+str(id)+"_black.csv")
    goals_green = pd.read_csv(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\Foosball\goals\24_fps\g"+str(id)+"_green.csv")
    
    goals_black = get_goals_float (goals_black)
    goals_green = get_goals_float(goals_green)
    
    return goals_black, goals_green

combine_f=[]
for r in range(11):#11 games with 24 fps
    g_b, g_g = load_goals(r+1)
    d = np.sort(np.concatenate((g_b, g_g)))
    v = [d[0]]
    #v=[]
    for k in range(len(d)-1):
        a = d[k+1]-d[k]
        v.append(a)
    combine_f = np.concatenate((combine_f, v))
    
combine = combine_f/fps

plt.figure()

distp=sns.distplot(combine,color='black')
font = {'size': 14, 'sans-serif':'Arial'} # define font
mpl.rc('font', **font) # use font

x,y = distp.lines[-1].get_data()
xmax = x[np.where(y == y.max())]
outer_xlim = x[np.where((np.cumsum(y)) < 0.9*(np.sum(y)))]
outer_xlim = outer_xlim[-1]

plt.xlim(0,int(outer_xlim+1))

plt.xlabel('Density')
plt.xlabel('Time [s]')
sns.despine()
#plt.savefig(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\images\Foosball\24fps\density.png", dpi=300,  bbox_inches='tight')
#plt.savefig(r"C:\Users\lilli\Documents\Bildung\Uni\aachen\Unterricht_Inhaltlich\AG Kampa\images\Foosball\24fps\density.svg", dpi=300,  bbox_inches='tight')
